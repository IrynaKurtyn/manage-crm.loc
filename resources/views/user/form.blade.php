@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>
                    {{$error}}
                </li>
            @endforeach
        </ul>
    </div>
@endif
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="row-fluid">
    <div class="span10">
        <div class="widget-box">
            <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>Personal-info</h5>
            </div>
            <div class="widget-content nopadding">

                {!! Form::open(['class' => 'form-horizontal' , 'method' => 'post', 'action' => 'UsersController@store', 'enctype' => 'multipart/form-data'])  !!}
                {{--<form action="#" method="get" class="form-horizontal">--}}
                <div class="control-group">
                    {!! Form::label('Name', null, ['class' => 'control-label']); !!}
                    <div class="controls">
                        {!! Form::text('name', null, ['class' => 'span11', 'placeholder' => 'First name && Last name']); !!}
                    </div>
                </div>
                <div class="control-group">
                    {!! Form::label('Email', null, ['class' => 'control-label']); !!}
                    <div class="controls">
                        {!! Form::email('email', null, ['class' => 'span11', 'placeholder' => 'Email']); !!}
                    </div>
                </div>
                <div class="control-group">
                    {!! Form::label('Password', null, ['class' => 'control-label']); !!}
                    <div class="controls">
                        {!! Form::text('password', null, ['class' => 'span11', 'placeholder' => 'Enter Password']); !!}
                    </div>
                </div>
                <div class="control-group">
                    {!! Form::label('Photo', null, ['class' => 'control-label']); !!}
                    <div class="controls">
                        {!! Form::file('photo') !!}
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                {{--</form>--}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
