<div class="container-fluid">
    <div class="row justify-content-center" style="margin-left: 20px">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        {!! $grid !!}
    </div>
</div>