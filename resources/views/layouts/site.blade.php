<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Matrix Admin</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/fullcalendar.css" />
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/matrix-style.css" />
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/matrix-media.css" />
    <link href="{{ asset(env('THEME')) }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <!-- progress bar (not required, but cool) -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.css" />
    <!-- bootstrap (required) -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" />
    <!-- date picker (required if you need date picker & date range filters) -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <!-- grid's css (required) -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/leantony/grid/css/grid.css') }}" />
</head>
<body>

<!--Header-part-->
@yield('header')

<!--close-top-serch-->
<!--sidebar-menu-->
@yield('sidebar')
<!--sidebar-menu-->

<!--main-container-part-->
@yield('content')

<!--end-main-container-part-->

<!--Footer-part-->
@yield('footer')


<!--end-Footer-part-->

<script src="{{ asset(env('THEME')) }}/js/excanvas.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.ui.custom.js"></script>
<script src="{{ asset(env('THEME')) }}/js/bootstrap.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.flot.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.flot.resize.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.peity.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/fullcalendar.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.dashboard.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.gritter.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.interface.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.chat.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.validate.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.form_validation.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.wizard.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.uniform.js"></script>
<script src="{{ asset(env('THEME')) }}/js/select2.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.popover.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.dataTables.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/matrix.tables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js"></script>
<!-- moment js (required by datepicker library) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<!-- jquery (required) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- popper js (required by bootstrap) -->
<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
<!-- bootstrap js (required) -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<!-- pjax js (required) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.pjax/2.0.1/jquery.pjax.min.js"></script>
<!-- datepicker js (required for datepickers) -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<!-- required to supply js functionality for the grid -->
<script src="{{ asset('vendor/leantony/grid/js/grid.js') }}"></script>
<script>
    // send csrf token (see https://laravel.com/docs/5.6/csrf#csrf-x-csrf-token) - this is required
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // for the progress bar (required for progress bar functionality)
    $(document).on('pjax:start', function () {
        NProgress.start();
    });
    $(document).on('pjax:end', function () {
        NProgress.done();
    });
</script>

<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
</body>
</html>
