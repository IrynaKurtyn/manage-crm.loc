<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends ParentController
{
    //
    public function index(){
        $content = view('dashboard.content')->render();
        return $this->renderOutPut($content);
    }
}
