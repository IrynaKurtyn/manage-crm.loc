<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParentController extends Controller
{
    //
    protected $template = 'common';
    protected $vars = [];

    public function renderOutPut($content = ''){
        $this->vars = array_add($this->vars, 'content', $content);
        return view($this->template)->with($this->vars);
    }
}
