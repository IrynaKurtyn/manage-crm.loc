<?php

namespace App\Http\Controllers;

use App\Grids\UsersGridInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;

class UsersController extends ParentController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersGridInterface $usersGrid, Request $request)
    {
        //
        $query = User::query();
        $content = $usersGrid
            ->create(['query' => $query, 'request' => $request])
            ->renderOn('user.index');
        return $this->renderOutPut($content);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = new User();
        $content = view('user.form')->with('user', $user)->render();
        return $this->renderOutPut($content);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user,Request $request)
    {
        //
        $input = $request->except('_token');
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
        ]);

        if($validator->fails()){
            return redirect()->route('users.create')->withErrors($validator);
        }
        if($request->hasFile('photo')){
            $file = $request->file('photo');
//            dd($file);
            $file->move(public_path().'/img', $file->getClientOriginalName());
            $input['photo'] = $file->getClientOriginalName();
        }

//        dd($input);

        $user->fill($input);
        if($user->save()){
            return redirect('users')->with('status', 'Successfully saved');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
//        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        dd($id);
        User::destroy($id);
        return redirect()->route("users.index");
    }

}
